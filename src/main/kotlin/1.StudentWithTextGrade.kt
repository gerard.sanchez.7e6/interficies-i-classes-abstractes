/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/02/28,
* TITLE: StudentWithTextGrade
*/


enum class Notes{
    FAILED,
    PASSED,
    GOOD,
    NOTABLE,
    EXCELENT

}
 data class Student(private val name: String, private val textGrade: Notes) {
    fun printStudent(): String{
        val mar = Student("Mar", Notes.FAILED)
        val joan = Student("Joan", Notes.EXCELENT)
        return "$mar \n$joan"
  }
}
fun main (){
    println(Student("", Notes.GOOD).printStudent())

}

