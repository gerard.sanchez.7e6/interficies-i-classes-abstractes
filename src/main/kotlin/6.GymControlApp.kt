import java.util.*

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/02/28
* TITLE: GymControlApp
*/
interface GymControlReader{
    fun nextId() : String
}
class GymControlManualReader(val scanner:Scanner = Scanner(System.`in`)) : GymControlReader {
    override fun nextId() = scanner.next()!!

}

