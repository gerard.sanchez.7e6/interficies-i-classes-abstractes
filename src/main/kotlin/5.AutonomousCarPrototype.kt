/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/02/28
* TITLE: AutonomousCarPrototype
*/
enum class Direction {
    FRONT,
    LEFT,
    RIGHT
}
interface CarSensors{
    fun isThereSomethingAt(direction: Direction) : Boolean
    fun go(direction : Direction)
    fun stop()

}
class AutonumousCar : CarSensors{
    fun doNextNSteps(n :Int){
        for (i in 1 ..n){
            if (!isThereSomethingAt(Direction.FRONT)){
                go(Direction.FRONT)
            }
            else{
                if (!isThereSomethingAt(Direction.LEFT)){
                    go(Direction.RIGHT)
            }
                if (!isThereSomethingAt(Direction.RIGHT)) {
                    go(Direction.LEFT)
            }
                else{
                    stop()
                }
            }
        }
    }

    override fun isThereSomethingAt(direction: Direction): Boolean {
        TODO("Not yet implemented")
    }

    override fun go(direction: Direction) {
        TODO("Not yet implemented")
    }

    override fun stop() {
        TODO("Not yet implemented")
    }
}

fun main(){
    val carPrototip = AutonumousCar()
    carPrototip.doNextNSteps(10)

}
