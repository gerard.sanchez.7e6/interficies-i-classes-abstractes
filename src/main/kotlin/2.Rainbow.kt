
import java.util.Scanner

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/02/28
* TITLE: Rainbow
*/

 enum class RainbowColors {
     VERMELL,
     TARONJA,
     GROC,
     VERD,
     BLAU,
     INDI,
     VIOLETA}

fun colorRainbow(color: String ): Boolean {
    return RainbowColors.values().any() { it.name == color }
}
fun main (){
    val scanner = Scanner(System.`in`)
    print("Introdueix un color: ")
    println(colorRainbow(scanner.next().uppercase()))

}
