import java.util.Scanner

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/02/28
* TITLE: Quiz
*/
abstract class Question(val sentence : String, val correctAnswer: String){
    abstract fun showText()
    abstract fun checkCorrectAnswer(userAnswer: String): String
}
class FreeTextQuestion(sentence: String, correctAnswer : String): Question(sentence, correctAnswer){
    override fun showText() {
        println(sentence)
    }
    override fun checkCorrectAnswer(userAnswer : String): String {
        return if ( userAnswer == correctAnswer){
            "Resposta correcta"
        } else {
            "Resposta incorrecta"
        }
    }
}
class MultipleChoiseQuestion(sentence: String,
                             correctAnswer: String,
                             private val firstIncorrectAnswer : String,
                             private val secondIncorrectAnswer : String,
                             private val thirdIncorrectAnswer : String) : Question(sentence, correctAnswer){
    override fun showText()  {
       println(sentence)
        val allAnswer = mutableListOf(firstIncorrectAnswer,secondIncorrectAnswer,thirdIncorrectAnswer,correctAnswer).shuffled()
        println("1-> ${allAnswer[0]}\n" +
                "2-> ${allAnswer[1]}\n" +
                "3-> ${allAnswer[2]}\n" +
                "4-> ${allAnswer[3]}")
    }
    override fun checkCorrectAnswer(userAnswer : String): String {
        return if ( userAnswer == correctAnswer){
            "Resposta correcta"
        } else {
            "Resposta incorrecta"
        }
    }

}
class Quiz() {
    val problems = mutableListOf(
        FreeTextQuestion("Quina és la capital d'Italia?", "Roma"),
        MultipleChoiseQuestion("Quantes pilotes d'or té Messi", "5","6","8","7"))
}



fun main(){
    val quiz = Quiz()
    val questions = quiz.problems
     for (i in questions){
         i.showText()
         val scanner = Scanner(System.`in`)
         val userAnswer = scanner.next()
         println(i.checkCorrectAnswer(userAnswer))

     }




}