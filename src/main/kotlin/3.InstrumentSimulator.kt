/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/02/28
* TITLE: InstrumentSimulator
*/
abstract class Instrument {
    abstract fun makeSound(): String
    fun makeSounds(times : Int) {
        repeat(times){ println(makeSound()) }
    }
}
class Drump(private val tone: String) : Instrument() {
     override fun makeSound(): String {
        return when(tone) {
            "A" -> "TAAAM"
            "O" -> "TOOOM"
            "U" -> "TUUUM"
            else -> ""
        }
    }
}
class Triangle(private val resonance: Int) : Instrument() {
    override fun makeSound(): String {
        return "T" + "I".repeat(resonance) + "NC"
    }
}
fun main() {
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drump("A"),
        Drump("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}

private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSounds(2)
    }
}

